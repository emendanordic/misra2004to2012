# Date: 2016-05-30
# Name: misraMap20042012.py
# Created by: Emenda
# Description: Creates a map from QAC-specific issue IDs to MISRA error codes
#	e.g. 3458 -> MISRA-C:2004 Rule 19.4
#================================================================================
# Changelog:
# Version 1.0 Initial version by Andreas Larfors


import sys,re,os, tempfile, shutil
import fileinput, string

class keyPair:
	def __init__(self, key, values):
		self.key = key
		self.values = values

class misraMap20042012:
	def __init__(self):
		self.keymap = []

	def getValues(self, key):
		for pair in self.keymap:
			if pair.key == key:
				return pair.values

	def extendValues(self, key, values):
		for pair in self.keymap:
			if pair.key == key:
				pair.values.extend(values)

	def loadMapFile(self, filename):
		key = ''
		values = []
		with open(filename, 'r') as f:
			for line in f:
				line = line.strip()
				if len(line) < 1:
					#next line will be a new key
					#store existing key and values if not stored already, else append
					if self.getValues(key) == None:
						keypair = keyPair(key, values)
						self.keymap.append(keypair)
					else:
						self.extendValues(key, values)
					key = ''
					values = []
				elif len(key) < 1:
					#new key
					key = "MISRA-C:2004 " + line
				else:
					#value - either a Rule, Dir or Deleted
					if line == 'Deleted':
						continue
					else:
						values.append("MISRA-C:2012 " + line)

def main():
	themap = misraMap20042012()
	themap.loadMapFile("misra-c_2004_to_2012_map-final")
	for pair in themap.keymap:
		print("key: " + pair.key)
		print("values: " + ','.join(pair.values))

if __name__ == '__main__':
	main()
